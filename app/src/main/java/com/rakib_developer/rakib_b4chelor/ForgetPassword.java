package com.rakib_developer.rakib_b4chelor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgetPassword extends AppCompatActivity {
    EditText editTextforgot;
    Button buttonforgotpassword;
    TextView textView;
    private ProgressDialog progressBar;
    private FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        textView=(TextView)findViewById(R.id.passwordsendconfor);
        editTextforgot=(EditText)findViewById(R.id.forgotemil);
        buttonforgotpassword=(Button)findViewById(R.id.forgotbutton);
        progressBar = new ProgressDialog(this);
        firebaseAuth = FirebaseAuth.getInstance();
        buttonforgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=  editTextforgot.getText().toString().trim();
                if (email.isEmpty()){

                    editTextforgot.setError("Email Write");
                    editTextforgot.requestFocus();
                    return;
                }
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

                    editTextforgot.setError("Email Not Valid");
                    editTextforgot.requestFocus();
                    return;
                }

                progressBar.setTitle("Forgot Password");
                progressBar.setMessage("Please wait,We are sending your Password email");
                progressBar.setIcon(R.drawable.ic_edit_black_24dp);
                progressBar.setCanceledOnTouchOutside(false);
                progressBar.show();
                forgotpass();
            }
        });
    }
    public void forgotpass(){
        String email=  editTextforgot.getText().toString().trim();
        firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    progressBar.dismiss();
                    textView.setText("Please Chack Your Email Address");
                    //Toast.makeText(login_page2.this, "Password Send To Your Email ", Toast.LENGTH_LONG).show();

                }
                else {
                    progressBar.dismiss();
                    Toast.makeText(ForgetPassword.this, "Error "+task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}
