package com.rakib_developer.rakib_b4chelor.Model_Class;

public class ModelUser {


    String email,uuid,number,name,image;


    public ModelUser()
    {

    }

    public ModelUser(String email, String uuid, String number, String name,String image) {
        this.email = email;
        this.uuid = uuid;
        this.number = number;
        this.name = name;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
