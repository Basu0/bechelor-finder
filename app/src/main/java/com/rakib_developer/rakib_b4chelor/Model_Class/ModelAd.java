package com.rakib_developer.rakib_b4chelor.Model_Class;

public class ModelAd {
    String key, name, info, roommate, roomfare, uuid, image,Number;

    public ModelAd() {
    }

    public ModelAd(String key, String name, String info, String roommate, String roomfare, String uuid, String image,String Number) {
        this.key = key;
        this.name = name;
        this.info = info;
        this.roommate = roommate;
        this.roomfare = roomfare;
        this.uuid = uuid;
        this.image = image;
        this.Number = Number;
    }


    public String getImage() {
        return image;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getRoommate() {
        return roommate;
    }

    public void setRoommate(String roommate) {
        this.roommate = roommate;
    }

    public String getRoomfare() {
        return roomfare;
    }

    public void setRoomfare(String roomfare) {
        this.roomfare = roomfare;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
