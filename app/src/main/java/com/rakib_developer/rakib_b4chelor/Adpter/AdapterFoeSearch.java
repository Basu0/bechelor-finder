package com.rakib_developer.rakib_b4chelor.Adpter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rakib_developer.rakib_b4chelor.Activity.Search_details_show;
import com.rakib_developer.rakib_b4chelor.Model_Class.ModelAd;
import com.rakib_developer.rakib_b4chelor.R;

import java.util.ArrayList;

public class AdapterFoeSearch extends RecyclerView.Adapter<AdapterFoeSearch.MyviewHolder> {
    Context context;
    ArrayList<ModelAd> searchArrayList;


    public AdapterFoeSearch(Context context, ArrayList<ModelAd> searchArrayList) {
        this.context = context;
        this.searchArrayList = searchArrayList;
    }


    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.postview_simple_activity, parent, false);
        MyviewHolder viewholderobj = new MyviewHolder(itemView);
        return viewholderobj;
    }


    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, final int position) {

        holder.roomMte.setText(searchArrayList.get(position).getRoommate());
        holder.roomRent.setText("" +searchArrayList.get(position).getRoomfare()+" Tk");
        holder.postbyname.setText(""+searchArrayList.get(position).getName()+"");
        holder.infotext.setText(searchArrayList.get(position).getInfo());
        Glide
                .with(context)
                .load(searchArrayList.get(position).getImage())
                .into(holder.roomImage);

        final ModelAd modelAd=searchArrayList.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent searchDetailsIntent = new Intent(context, Search_details_show.class);

                searchDetailsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                searchDetailsIntent.putExtra("roomMte",searchArrayList.get(position).getRoommate());
                searchDetailsIntent.putExtra("roomRent",searchArrayList.get(position).getRoomfare());
                searchDetailsIntent.putExtra("roomImage1",searchArrayList.get(position).getImage());
                searchDetailsIntent.putExtra("Myname",searchArrayList.get(position).getName());
                searchDetailsIntent.putExtra("information",searchArrayList.get(position).getInfo());
                searchDetailsIntent.putExtra("uuid",searchArrayList.get(position).getUuid());
                searchDetailsIntent.putExtra("Phone",searchArrayList.get(position).getNumber());
                context.startActivity(searchDetailsIntent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return searchArrayList.size();
    }


    public class MyviewHolder extends RecyclerView.ViewHolder {

        TextView roomMte, roomRent,postbyname,infotext;
        ImageView roomImage;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            roomMte = itemView.findViewById(R.id.room_mate_number);
            roomRent = itemView.findViewById(R.id.room_rent);
            roomImage = itemView.findViewById(R.id.notifcation_iimage);
            postbyname = itemView.findViewById(R.id.PostbyName);
            infotext=itemView.findViewById(R.id.infotext);
        }
    }

    public void setValue(ModelAd modelAd)
    {
        this.searchArrayList.add(0,modelAd);
        notifyDataSetChanged();
    }
}
