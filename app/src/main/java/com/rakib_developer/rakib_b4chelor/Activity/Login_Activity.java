package com.rakib_developer.rakib_b4chelor.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.rakib_developer.rakib_b4chelor.ForgetPassword;
import com.rakib_developer.rakib_b4chelor.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static maes.tech.intentanim.CustomIntent.customType;

public class Login_Activity extends AppCompatActivity {
   Button RegBtton,LoginBt;
    private TextView tvLogin;
    EditText EamilEdir,PasswordEdit;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressBar;
    FirebaseUser user;
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);
        RegBtton=findViewById(R.id.btRegister);
        tvLogin     = findViewById(R.id.tvLogin);
        LoginBt=findViewById(R.id.loginbt);
        EamilEdir=findViewById(R.id.EmailETText);
        PasswordEdit=findViewById(R.id.PasswordETText);
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseApp.initializeApp(this);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        progressBar = new ProgressDialog(this);


        LoginBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String email=EamilEdir.getText().toString().trim();
                String password=PasswordEdit.getText().toString().trim();
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

                    EamilEdir.setError("Email Not Valid");
                    EamilEdir.requestFocus();
                    return;
                }

                if (password.isEmpty()){

                    PasswordEdit.setError("Password Null");
                    PasswordEdit.requestFocus();
                    return;
                }
                if (password.length()<6){
                    PasswordEdit.setError("Minimum Password 6 ");
                    PasswordEdit.requestFocus();
                    return;
                }
                else{
                    progressBar.setTitle("Login");
                    progressBar.setMessage("Please wait,We are Account Login");
                    progressBar.setCanceledOnTouchOutside(false);
                    progressBar.show();
                    Login();
                }
            }
        });

        RegBtton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                if (view==RegBtton){
                    Intent intent   = new Intent(Login_Activity.this, RegisterActivity.class);
                    Pair[] pairs    = new Pair[1];
                    pairs[0] = new Pair<View,String>(tvLogin,"tvLogin");
                    ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(Login_Activity.this,pairs);
                    startActivity(intent,activityOptions.toBundle());
                }
            }

        });

    }

    public void Login(){

        String email=EamilEdir.getText().toString().trim();
        String password=PasswordEdit.getText().toString().trim();
        firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new
                OnCompleteListener<AuthResult>() {


                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()){
                            progressBar.dismiss();

                            sp.edit().putBoolean("logged",true).apply();
                            startActivity(new Intent(Login_Activity.this,Home_Activity.class));
                            customType(Login_Activity.this,"fadein-to-fadeout");
                            finish();
                            FirebaseUser user=firebaseAuth.getCurrentUser();

                        }

                        else {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException){
                                progressBar.dismiss();

                                new SweetAlertDialog(Login_Activity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops...")
                                        .setContentText("Already Login")
                                        .show();

                            }
                            else {
                                new SweetAlertDialog(Login_Activity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops...")
                                        .setContentText("Something went wrong!")
                                        .show();
                                progressBar.dismiss();

                            }


                        }
                    }
                });
    }

    public void forgetpasswor(View view) {
        startActivity(new Intent(Login_Activity.this, ForgetPassword.class));
        customType(Login_Activity.this,"fadein-to-fadeout");
    }
}
