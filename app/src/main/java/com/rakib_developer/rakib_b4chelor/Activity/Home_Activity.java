package com.rakib_developer.rakib_b4chelor.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rakib_developer.rakib_b4chelor.Adpter.AdapterFoeSearch;
import com.rakib_developer.rakib_b4chelor.Adpter.ProfileSearchAdapter;
import com.rakib_developer.rakib_b4chelor.Model_Class.ModelAd;
import com.rakib_developer.rakib_b4chelor.Model_Class.ModelUser;
import com.rakib_developer.rakib_b4chelor.R;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static maes.tech.intentanim.CustomIntent.customType;

public class Home_Activity extends AppCompatActivity {
    private static final int PICK_IMAGE_REQUEST = 11;
    private static final int RC_PERMISSION_READ_EXTERNAL_STORAGE = 12;
    ImageView profileImage;
    TextView profileUsername, myPosts, profileholdernumber;
    private  long mLastClickTime = 0;
    RecyclerView profileRecylerview;
    FirebaseAuth mAuth;
    RecyclerView recylerForsearch;
    ArrayList<ModelAd> searchList,profilelist;
    FirebaseDatabase database;
    AdapterFoeSearch searchadapter;
    ProfileSearchAdapter progileSerach;

    DatabaseReference reference,reference2;
    SwipeRefreshLayout swipeRefresh;
    SwipeRefreshLayout refreshprofile;
    private ProgressDialog progressBar;
    LinearLayout AllpostLayout,ProfileLayout;
    SharedPreferences sp;
    private StorageReference mStorageRef;
    ModelUser user;
    Uri mImageUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        mAuth = FirebaseAuth.getInstance();
        recylerForsearch = findViewById(R.id.recyclerview_search_list);
        profileRecylerview=findViewById(R.id.recyclerview_for_prifle);

        AllpostLayout=findViewById(R.id.allpostlayout);
        ProfileLayout=findViewById(R.id.profilelayout);

        database = FirebaseDatabase.getInstance();
        reference = database.getReference("posts");
        reference2 = database.getReference("AccountCreate_Table");
        mStorageRef = FirebaseStorage.getInstance().getReference("uploads");

        profileImage = findViewById(R.id.profileaccount_picture);
        profileUsername = findViewById(R.id.user_profilenametext);
        profileholdernumber = findViewById(R.id.user_cellnumber);

        searchList = new ArrayList<>();
        profilelist = new ArrayList<>();

        swipeRefresh=findViewById(R.id.swipe_refresh);
        refreshprofile=findViewById(R.id.refreshprofile);
        progressBar = new ProgressDialog(this);
        recylerForsearch.setLayoutManager(new LinearLayoutManager(this));
        profileRecylerview.setLayoutManager(new LinearLayoutManager(this));

        searchadapter = new AdapterFoeSearch(getApplicationContext(), searchList);
        progileSerach = new ProfileSearchAdapter(getApplicationContext(), profilelist);

        recylerForsearch.setAdapter(searchadapter);
        profileRecylerview.setAdapter(progileSerach);

        progressBar.setTitle("Loading");
        progressBar.setMessage("Please wait....");
        progressBar.show();
        getAllData();
        profiledataShow();
        getAllDataProfile();
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllData();
                progressBar.dismiss();
            }
        });

        refreshprofile.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllDataProfile();
                progressBar.dismiss();
            }
        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileChooser();
            }
        });

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_home:
                        getAllData();
                        AllpostLayout.setVisibility(View.VISIBLE);
                        ProfileLayout.setVisibility(View.GONE);
                       break;

                }


                switch (menuItem.getItemId()) {
                    case R.id.actionAdd:
                       startActivity(new Intent(Home_Activity.this,Post_Activity.class));
                       customType(Home_Activity.this,"fadein-to-fadeout");

                        break;

                }
                switch (menuItem.getItemId()) {
                    case R.id.action_Profile:
                        getAllDataProfile();
                        refreshprofile.setRefreshing(false);
                        ProfileLayout.setVisibility(View.VISIBLE);
                        AllpostLayout.setVisibility(View.GONE);

                        break;

                }
                return true;
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu2, menu);
        menu.getItem(0).setIcon(R.drawable.ic_exit_to_app_black_24dp);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logout:
                new AlertDialog.Builder(Home_Activity.this)
                        .setTitle("Log out")
                        .setMessage("Are you Sure ?")
                        .setIcon(R.drawable.ic_exit_to_app_black_24dp)
                        .setPositiveButton("YES",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(11)
                                    public void onClick(DialogInterface dialog, int id) {
                                        FirebaseAuth.getInstance().signOut();
                                        goToMainActivity();
                                        sp.edit().putBoolean("logged",false).apply();
                                        finish();

                                        dialog.cancel();
                                    }
                                })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getAllData() {

        Query query = reference.orderByKey();
        //Query query = reference.orderByChild("uuid").equalTo(mAuth.getCurrentUser().getUid());
        searchList.clear();
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                ModelAd ad = dataSnapshot.getValue(ModelAd.class);
                searchadapter.setValue(ad);
                swipeRefresh.setRefreshing(false);
                progressBar.dismiss();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void getAllDataProfile() {
        profilelist.clear();
        //Query query = reference.orderByKey();
       Query query = reference.orderByChild("uuid").equalTo(mAuth.getCurrentUser().getUid());

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                ModelAd ad = dataSnapshot.getValue(ModelAd.class);
                progileSerach.setValue(ad);
                refreshprofile.setRefreshing(false);
                progressBar.dismiss();

                if (ad==null){
                    refreshprofile.setRefreshing(false);

                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                progressBar.dismiss();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                progressBar.dismiss();
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                progressBar.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressBar.dismiss();
            }
        });

    }

    public void ProfilePicUpload(View view) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 10000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        if (mImageUri==null) {

            new SweetAlertDialog(Home_Activity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Profile image Click")
                    .show();
        }
        else {
            progressBar.setTitle("Upload Profile Image");
            progressBar.setMessage("Please wait....");
            progressBar.show();

        uploadImage();
        }
    }

    public void goToMainActivity(){
        Intent i = new Intent(this,Login_Activity.class);
        startActivity(i);
        finish();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RC_PERMISSION_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_IMAGE_REQUEST);
            }
        }
    }
    private void openFileChooser() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, RC_PERMISSION_READ_EXTERNAL_STORAGE);
        } else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, PICK_IMAGE_REQUEST);
        }
    }
    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            mImageUri = data.getData();
            //mImageView.setImageURI(mImageUri);

            Glide.
                    with(getApplicationContext())
                    .load(mImageUri)
                    .into(profileImage);
        }


    }
    private void uploadImage() {


        final StorageReference fileReference = mStorageRef.child("images").child(System.currentTimeMillis() + "_" + FirebaseAuth.getInstance().getUid()
                + "." + getFileExtension(mImageUri));

        fileReference.putFile(mImageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        fileReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                user.setImage(uri.toString());
                                reference2.child(user.getUuid()).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        progressBar.dismiss();
                                        new SweetAlertDialog(Home_Activity.this, SweetAlertDialog.SUCCESS_TYPE)
                                                .setTitleText("Good Job")
                                                .setContentText("Image Uploade Success")
                                                .show();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {

                                    }
                                });
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        });


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                    }
                });
    }


    public void profiledataShow(){

        reference2.child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(ModelUser.class);
        String image="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1024px-Circle-icons-profile.svg.png";

                if (user.getImage()==null){
                    Glide.
                            with(getApplicationContext())
                            .load(image)
                            .into(profileImage);

                }
          else {
                Glide.
                        with(getApplicationContext())
                        .load(user.getImage())
                        .into(profileImage);}

                profileUsername.setText(user.getName());
                profileholdernumber.setText(user.getNumber());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
