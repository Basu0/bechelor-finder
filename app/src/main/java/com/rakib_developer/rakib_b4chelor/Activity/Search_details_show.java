package com.rakib_developer.rakib_b4chelor.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rakib_developer.rakib_b4chelor.R;

public class Search_details_show extends AppCompatActivity {

    TextView searchMyname,number,searchinformation,searchroomMte,searchroomRent;
    ImageView imng;
    String roomMte,roomRent,Myname,information,pnnumber,uuid,imageu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search_details_show);

        searchMyname = findViewById(R.id.search_username);
        number = findViewById(R.id.searchdetails_number);
        searchinformation = findViewById(R.id.search_information);
        searchroomMte = findViewById(R.id.search_roommate);
        searchroomRent = findViewById(R.id.search_rent);
        imng=findViewById(R.id.search_img);

         roomMte = getIntent().getStringExtra("roomMte");
         roomRent = getIntent().getStringExtra("roomRent");
          Myname = getIntent().getStringExtra("Myname");
         information = getIntent().getStringExtra("information");
         uuid = getIntent().getStringExtra("uuid");
         pnnumber = getIntent().getStringExtra("Phone");
         imageu=getIntent().getStringExtra("roomImage1");

        searchMyname.setText("AD owner : " +Myname);
        searchinformation.setText("Info : " +information);
        searchroomMte.setText(roomMte);
        searchroomRent.setText(""+roomRent+" Tk");
        number.setText(""+pnnumber);
        Glide.with(this).load(imageu).into(imng);
    }

    private void dialContactPhone(final String phoneNumber) {
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null)));
    }


    public void callnumber(){
        final String phonenumber=number.getText().toString();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("CAll");
        alertDialogBuilder.setIcon(android.R.drawable.ic_menu_call);
        alertDialogBuilder.setMessage("Phone Number:"+phonenumber+"");


        alertDialogBuilder.setPositiveButton("Call Now",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        dialContactPhone(""+phonenumber+"");



                    }
                });

        alertDialogBuilder.setNegativeButton("No Call",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void callnow(View view) {
        callnumber();
    }
}
