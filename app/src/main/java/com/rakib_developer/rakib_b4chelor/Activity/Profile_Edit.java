package com.rakib_developer.rakib_b4chelor.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.rakib_developer.rakib_b4chelor.Model_Class.ModelAd;
import com.rakib_developer.rakib_b4chelor.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Profile_Edit extends AppCompatActivity {
    EditText searchMyname,searchinformation,searchroomMte,searchroomRent;
    ImageView imng;
    String roomMte,roomRent,Myname,information,pnnumber,uuid,imageu,keyid;
    FirebaseDatabase firebaseDatabase;
    private ProgressDialog progressBar;
    DatabaseReference databaseReference;
   ModelAd modelAd;
    FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile__edit);

        searchMyname = findViewById(R.id.search_username);
        searchinformation = findViewById(R.id.search_information);
        searchroomMte = findViewById(R.id.search_roommate);
        searchroomRent = findViewById(R.id.search_rent);
        imng=findViewById(R.id.search_img);

        roomMte = getIntent().getStringExtra("roomMte");
        roomRent = getIntent().getStringExtra("roomRent");
        Myname = getIntent().getStringExtra("Myname");
        information = getIntent().getStringExtra("information");
        uuid = getIntent().getStringExtra("uuid");
        pnnumber = getIntent().getStringExtra("Phone");
        imageu=getIntent().getStringExtra("roomImage1");
        keyid=getIntent().getStringExtra("keyid");

        searchMyname.setText("" +Myname);
        searchinformation.setText("" +information);
        searchroomMte.setText(roomMte);
        searchroomRent.setText(""+roomRent+"");
        Glide.with(this).load(imageu).into(imng);

        progressBar = new ProgressDialog(this);
       // databaseReference= FirebaseDatabase.getInstance().getReference("posts");
        mAuth = FirebaseAuth.getInstance();
    }

    public void close(View view) {
        finish();
    }

    public void UpdateData(View view) {

        dataupdate();
    }


    public void dataupdate(){
        modelAd=new ModelAd();
        final String Name=searchMyname.getText().toString();
        final String Information=searchinformation.getText().toString();
        final String roommet=searchroomMte.getText().toString();
        final String rent=searchroomRent.getText().toString();
        new AlertDialog.Builder(Profile_Edit.this)
                .setTitle("Update")
                .setMessage("Please Wait....")
                .setIcon(R.drawable.ic_edit_black_24dp)
                .setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {
                                progressBar.setTitle("Upate");
                                progressBar.setMessage("Please wait..");
                                progressBar.show();
                                firebaseDatabase = FirebaseDatabase.getInstance();
                                databaseReference = firebaseDatabase.getReference();
                                databaseReference.child("posts").child(keyid).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {


                                        dataSnapshot.getRef().child("name").setValue(Name);
                                        dataSnapshot.getRef().child("info").setValue(Information);
                                        dataSnapshot.getRef().child("roommate").setValue(roommet);
                                        dataSnapshot.getRef().child("roomfare").setValue(rent);
                                        dataSnapshot.getRef().child("image").setValue(imageu);
                                        dataSnapshot.getRef().child("key").setValue(keyid);
                                        dataSnapshot.getRef().child("number").setValue(pnnumber);
                                        dataSnapshot.getRef().child("uuid").setValue(uuid);
                                        progressBar.dismiss();

                                        new SweetAlertDialog(Profile_Edit.this, SweetAlertDialog.SUCCESS_TYPE)
                                                .setTitleText("Update Success")
                                                .setContentText("Update Data !")
                                                .show();
                                        //Toast.makeText(getApplication(),"Update Success",Toast.LENGTH_LONG).show();

                                        //inlist.clear();


                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Toast.makeText(getApplication(),"Error"+databaseError,Toast.LENGTH_LONG).show();
                                        progressBar.dismiss();
                                    }
                                });

                                dialog.cancel();
                            }
                        })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                }).show();

    }

    public void DeleteData(View view) {
        new AlertDialog.Builder(Profile_Edit.this)
                .setTitle("Delete")
                .setMessage("Are you sure ?")
                .setIcon(R.drawable.ic_delete_sweep_black_24dp)
                .setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {
                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                                Query applesQuery = ref.child("posts").orderByChild("key").equalTo(keyid);

                                applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                                            appleSnapshot.getRef().removeValue();
                                            new SweetAlertDialog(Profile_Edit.this, SweetAlertDialog.SUCCESS_TYPE)
                                                    .setTitleText("Oops...")
                                                    .setContentText("Delete Data !")
                                                    .show();
                                            //finish();

                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        new SweetAlertDialog(Profile_Edit.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Oops...")
                                                .setContentText(""+databaseError)
                                                .show();

                                    }
                                });

                                dialog.cancel();
                            }
                        })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                }).show();
    }
}
