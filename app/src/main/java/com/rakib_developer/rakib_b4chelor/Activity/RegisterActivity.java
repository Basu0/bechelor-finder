package com.rakib_developer.rakib_b4chelor.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rakib_developer.rakib_b4chelor.R;

import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static maes.tech.intentanim.CustomIntent.customType;

public class RegisterActivity extends AppCompatActivity {
    private RelativeLayout rlayout;
    private Animation animation;
    EditText EmailET,NameET,PasswordET,RetypeET,PhoneET;
    Button BTReg;
    SweetAlertDialog pDialog;
    String userName, userEmail, userNumber, userPass, re_userPass;
    SharedPreferences sp;
    private FirebaseAuth mAuth;
    FirebaseDatabase database;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Toolbar toolbar = findViewById(R.id.bgHeader);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAuth = FirebaseAuth.getInstance();
        database=FirebaseDatabase.getInstance();
        reference=database.getReference("AccountCreate_Table");
        sp = getSharedPreferences("login",MODE_PRIVATE);

        EmailET=findViewById(R.id.emailET);
        NameET=findViewById(R.id.nameET);
        PasswordET=findViewById(R.id.passwordET);
        RetypeET=findViewById(R.id.retypeET);
        PhoneET=findViewById(R.id.phoneET);
        BTReg=findViewById(R.id.RegisterBT);

        rlayout = findViewById(R.id.rlayout);
        animation = AnimationUtils.loadAnimation(this,R.anim.uptodowndiagonal);
        rlayout.setAnimation(animation);

        BTReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email=EmailET.getText().toString().trim();
                String password=PasswordET.getText().toString().trim();
                String passwordRetype=RetypeET.getText().toString().trim();
                String Name=NameET.getText().toString();
                String Phone=PhoneET.getText().toString();
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

                    EmailET.setError("Email not valid !");
                    EmailET.requestFocus();
                    return;
                }
                if (Name.isEmpty()){
                    NameET.setError("Name enter");
                    NameET.requestFocus();
                    return;
                }
                if (password.isEmpty()){
                    PasswordET.setError("Password enter");
                    PasswordET.requestFocus();
                    return;
                }

                if (passwordRetype.isEmpty()){
                    RetypeET.setError("ReType Password enter");
                    RetypeET.requestFocus();
                    return;
                }

                if (password.length()<6){
                    PasswordET.setError("Minimum Password 6 ");
                    PasswordET.requestFocus();
                    return;

                }
                if (!password.equals(passwordRetype)){

                    RetypeET.setError("Password Not Match ");
                    RetypeET.requestFocus();
                    return;
                }


                if (Phone.isEmpty()){
                    PhoneET.setError("Phone number enter");
                    PhoneET.requestFocus();
                    return;
                }
                else {
                 RegestionMathod();

                }
            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home :
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void RegestionMathod(){


        pDialog = new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading Please wait");
        pDialog.setCancelable(false);
        pDialog.show();

        String email=EmailET.getText().toString().trim();
        String password=PasswordET.getText().toString().trim();
       mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
         @Override
         public void onComplete(@NonNull Task<AuthResult> task) {
             if (task.isSuccessful()){
                 FirebaseUser user=mAuth.getCurrentUser();
                 String email=user.getEmail();
                 String Uid=user.getUid();
                 String Name=NameET.getText().toString();
                 String Phone=PhoneET.getText().toString();
                 HashMap<Object,String> hashMap=new HashMap<>();
                 hashMap.put("name",Name);
                 hashMap.put("email",email);
                 hashMap.put("number",Phone);
                 hashMap.put("uuid",Uid);
                 //hashMap.put("image","");
                 reference.child(Uid).setValue(hashMap);
                 pDialog.dismiss();
                 new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                         .setTitleText("Good Job")
                         .setContentText("Your Account Create")
                         .setConfirmText("Yes")
                         .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                             @Override
                             public void onClick(SweetAlertDialog sDialog) {

                                 sp.edit().putBoolean("logged",true).apply();
                                 startActivity(new Intent(RegisterActivity.this,Home_Activity.class));
                                 customType(RegisterActivity.this,"fadein-to-fadeout");
                                 finish();
                             }
                         }).show();
             }
             else  {
                 if (task.getException() instanceof FirebaseAuthUserCollisionException) {

                     new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.WARNING_TYPE)
                             .setTitleText("Already Registration")
                             .setContentText("Your Account Already Add ! ")
                             .setConfirmText("Yes")
                             .show();
                     pDialog.dismiss();




                 }
                 else {
                     new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                             .setTitleText("Oops...")
                             .setContentText("Something went wrong!")
                             .show();

                     //Toast.makeText(getApplication(),"Error :"+ task.getException().getMessage(),Toast.LENGTH_LONG).show();
                     pDialog.dismiss();
                 }
             }

         }
     });

        /*userName = NameET.getText().toString();
        userEmail = EmailET.getText().toString();
        userPass = PasswordET.getText().toString();
        re_userPass = RetypeET.getText().toString();
        userNumber = PhoneET.getText().toString();

        mAuth.createUserWithEmailAndPassword(userEmail, userPass)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {

                        ModelUser user = new ModelUser(userEmail,mAuth.getCurrentUser().getUid(),userNumber,userName,null);

                        reference.child(mAuth.getCurrentUser().getUid()).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                                pDialog.dismiss();
                                new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Good Job")
                                        .setContentText("Your Account Create")
                                        .setConfirmText("Yes")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {

                                                sp.edit().putBoolean("logged",true).apply();
                                                startActivity(new Intent(RegisterActivity.this,Home_Activity.class));
                                                customType(RegisterActivity.this,"fadein-to-fadeout");
                                                finish();
                                            }
                                        }).show();

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops...")
                                        .setContentText("Something went wrong!")
                                        .show();
                                pDialog.dismiss();
                            }
                        });



                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("WhatIS",e.getMessage());
            }
        });*/


    }

}
