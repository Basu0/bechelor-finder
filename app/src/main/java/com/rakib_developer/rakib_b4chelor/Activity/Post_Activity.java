package com.rakib_developer.rakib_b4chelor.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rakib_developer.rakib_b4chelor.Model_Class.ModelAd;
import com.rakib_developer.rakib_b4chelor.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Post_Activity extends AppCompatActivity {

    int counter = 0;
    Button PostAdbtn;
    TextView roommatenumber;
    EditText information, myname, rent, roomphone;

    FirebaseAuth mAuth;
    String information_data, myname_data, rent_data, roommatenumber_data,Number;

    ImageView image;
    private ProgressDialog progressBar;
    FirebaseDatabase database;
    DatabaseReference reference;
    private StorageReference mStorageRef;

    Uri uri;

    int READ_STORAGE_PERMISSION = 11;
    int gallery_code = 12;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Post ADD");

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference("uploads");
        reference = database.getReference("posts");
        progressBar = new ProgressDialog(this);

        myname = findViewById(R.id.username_postadd);
        information = findViewById(R.id.room_post_infomaation);
        roommatenumber=findViewById(R.id.quentityvalu);
        rent = findViewById(R.id.room_post_fare);
        image = findViewById(R.id.imageviwe_postad);
        roomphone=findViewById(R.id.numberEntry);

    }

    public void valuIncrimentBT(View view) {
        counter = counter + 1;
        roommatenumber.setText(String.valueOf(counter));
    }

    public void decrement(View view) {
        counter = counter - 1;
        roommatenumber.setText(String.valueOf(counter));
    }


    public void SubmitBT(View view) {
        myname_data = myname.getText().toString();
        information_data = information.getText().toString();
        rent_data = rent.getText().toString();
        roommatenumber_data = roommatenumber.getText().toString();
        Number=roomphone.getText().toString().trim();
        if (myname_data.isEmpty()){
            myname.setError("Name enter");
            myname.requestFocus();
            return;
        }
        if (information_data.isEmpty()){
            information.setError("Room Condition Enter");
            information.requestFocus();
            return;
        }
        if (rent_data.isEmpty()){
            rent.setError("Room Cost Enter");
            rent.requestFocus();
            return;
        }
        if (Number.isEmpty()){
            roomphone.setError("Phone number enter");
            roomphone.requestFocus();
            return;
        }
     if (uri==null){

         new SweetAlertDialog(Post_Activity.this, SweetAlertDialog.ERROR_TYPE)
                 .setTitleText("Oops...")
                 .setContentText("Image Uploade")
                 .show();
     }
     else {
         progressBar.setTitle("Upload Post");
         progressBar.setMessage("Please wait....");
         progressBar.show();

        uploadImage();
     }
    }
    private void openGallery() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_STORAGE_PERMISSION);
        } else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, gallery_code);
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == READ_STORAGE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, gallery_code);
            } else {
                Toast.makeText(this, "Permission Not Granted", Toast.LENGTH_SHORT).show();
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == gallery_code && resultCode == RESULT_OK) {
            if (data.getDataString() != null) {
                uri = data.getData();

                Glide
                        .with(this)
                        .load(uri)
                        .into(image);
            }
        }
    }
    private void insertIntoDatabase(String url) {

        DatabaseReference localReference = reference;

        final String key = localReference.push().getKey();
        //final String key = mAuth.getCurrentUser().getEmail();

        ModelAd ad = new ModelAd(key,myname_data,information_data,roommatenumber_data,rent_data,mAuth.getCurrentUser().getUid(),url,Number);


        localReference.child(key).setValue(ad).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                progressBar.dismiss();
                new SweetAlertDialog(Post_Activity.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Good Job")
                        .setContentText("Upload ok")
                        .show();
                //Toast.makeText(Post_Activity.this, "Success", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.dismiss();
                new SweetAlertDialog(Post_Activity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops..")
                        .setContentText("Upload Faild")
                        .show();
            }
        });

    }
    void uploadImage() {
        final StorageReference riversRef = mStorageRef.child("posts").child(System.currentTimeMillis() + "_" + FirebaseAuth.getInstance().getUid()
                + "." + getFileExtension());

                riversRef.putFile(uri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                insertIntoDatabase(uri.toString());
                            }
                        });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Toast.makeText(Post_Activity.this, "Failed", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    String getFileExtension() {
        ContentResolver rv = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(rv.getType(uri));
    }

    public void ImageOpen(View view) {
        openGallery();
    }
}
