package com.rakib_developer.rakib_b4chelor.Splash_screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.transition.Slide;
import android.view.Gravity;
import android.view.animation.DecelerateInterpolator;

import com.hololo.tutorial.library.Step;
import com.hololo.tutorial.library.TutorialActivity;
import com.rakib_developer.rakib_b4chelor.Activity.Home_Activity;
import com.rakib_developer.rakib_b4chelor.Activity.Login_Activity;
import com.rakib_developer.rakib_b4chelor.R;

import static maes.tech.intentanim.CustomIntent.customType;

public class Onboarding_screen extends TutorialActivity {
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        setAnimation();
        setCancelText("Skip");
        setFinishText("OK");
        if (
                sp.getBoolean("logged",false))

        {
            startActivity(new Intent(Onboarding_screen.this, Home_Activity.class));
            customType(Onboarding_screen.this,"up-to-bottom");


        }
        else {



        addFragment(new Step.Builder().setTitle("Booking Now")
                .setContent("Sign up to enjoy amazing offers")
                .setBackgroundColor(Color.parseColor("#FF8B3E")) // int background color
                .setDrawable(R.drawable.screen1) // int top drawable
                .build());

        addFragment(new Step.Builder().setTitle("Flight")
                .setContent("Room booking ")
                .setBackgroundColor(Color.parseColor("#FF8B3E")) // int background color
                .setDrawable(R.drawable.screen2) // int top drawable
                .build());
        addFragment(new Step.Builder().setTitle("Booking is Now Easy")

                .setContent("Sign up to enjoy amazing offers")
                .setBackgroundColor(Color.parseColor("#FF8B3E")) // int background color
                .setDrawable(R.drawable.screen3) // int top drawable
                //.setSummary("This is summary")
                .build());

        }


    }

    @Override
    public void currentFragmentPosition(int position) {
        //Toast.makeText(this, "Position : " + position, Toast.LENGTH_SHORT).show();

    }
    @Override
    public void finishTutorial() {
        startActivity(new Intent(Onboarding_screen.this,Login_Activity.class));
        customType(Onboarding_screen.this,"fadein-to-fadeout");

        finish();
    }

    public void setAnimation() {
        if (Build.VERSION.SDK_INT > 20) {
            Slide slide = new Slide();
            slide.setSlideEdge(Gravity.LEFT);
            slide.setDuration(400);
            slide.setInterpolator(new DecelerateInterpolator());
            getWindow().setExitTransition(slide);
            getWindow().setEnterTransition(slide);
        }
    }
}
