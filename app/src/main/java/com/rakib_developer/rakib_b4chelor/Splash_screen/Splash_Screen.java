package com.rakib_developer.rakib_b4chelor.Splash_screen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.rakib_developer.rakib_b4chelor.Activity.Home_Activity;
import com.rakib_developer.rakib_b4chelor.Activity.Login_Activity;
import com.rakib_developer.rakib_b4chelor.R;

import static maes.tech.intentanim.CustomIntent.customType;

public class Splash_Screen extends AppCompatActivity {
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        final Intent intent=new Intent(Splash_Screen.this, Login_Activity.class);

        Thread thread=new Thread(){
            public void run(){

                try {
                    sleep(2500);
                }catch (InterruptedException e){
                    e.printStackTrace();

                }
                finally {
                    if (
                            sp.getBoolean("logged",false))

                    {
                        startActivity(new Intent(Splash_Screen.this, Home_Activity.class));
                        customType(Splash_Screen.this,"up-to-bottom");


                    }
                    else {
                        startActivity(new Intent(Splash_Screen.this, Login_Activity.class));
                        customType(Splash_Screen.this,"up-to-bottom");

                    }
                    finish();
                }
            }

        };
        thread.start();

    }
}
